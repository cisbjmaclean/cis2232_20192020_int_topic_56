package cis.bo;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author bjmac
 */
public class TireJobJUnitTest {

    private TireJob tireJob;

    public TireJobJUnitTest() {
        tireJob = new TireJob();
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        tireJob = new TireJob();

    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    // ALSO MUST START WITH test    !!!!!
    //
    @Test
    public void testSetDiscountRateTireQuantity_3Tires_5Percent() {

        tireJob.setNumberOfTiresBeingChanged(3);
        tireJob.setDiscountRateTireQuantity();
        double actual = tireJob.getDiscountRateTireQuantity();
        assertEquals(0.05, actual);
    }

    @Test
    public void testSetDiscountRateTireQuantity_4Tires_5Percent() {

        tireJob.setNumberOfTiresBeingChanged(4);
        tireJob.setDiscountRateTireQuantity();
        double actual = tireJob.getDiscountRateTireQuantity();
        assertEquals(0.05, actual);
    }

    @Test
    public void testSetDiscountRateTireQuantity_2Tires_0Percent() {

        tireJob.setNumberOfTiresBeingChanged(2);
        tireJob.setDiscountRateTireQuantity();
        double actual = tireJob.getDiscountRateTireQuantity();
        assertEquals(0.0, actual);
    }

    @Test
    public void testSetDiscountRateCustomerType_1000_10Percent() {

        tireJob.setCustomerNumber(1000);
        tireJob.setDiscountRateCustomerType();
        double actual = tireJob.getDiscountRateCustomerType();
        assertEquals(0.1, actual);
    }

    @Test
    public void testSetDiscountRateCustomerType_999_0Percent() {

        tireJob.setCustomerNumber(999);
        tireJob.setDiscountRateCustomerType();
        double actual = tireJob.getDiscountRateCustomerType();
        assertEquals(0.0, actual);
    }

    @Test
    public void testSetDiscountRateCustomerType_1001_10Percent() {

        tireJob.setCustomerNumber(1001);
        tireJob.setDiscountRateCustomerType();
        double actual = tireJob.getDiscountRateCustomerType();
        assertEquals(0.1, actual);
    }
    

    @Test
    public void testSetDiscountRateCustomerType_1999_10Percent() {

        tireJob.setCustomerNumber(1999);
        tireJob.setDiscountRateCustomerType();
        double actual = tireJob.getDiscountRateCustomerType();
        assertEquals(0.1, actual);
    }    

    @Test
    public void testSetDiscountRateCustomerType_2000_20Percent() {

        tireJob.setCustomerNumber(2000);
        tireJob.setDiscountRateCustomerType();
        double actual = tireJob.getDiscountRateCustomerType();
        assertEquals(0.2, actual);
    }    
    @Test
    public void testSetDiscountRateCustomerType_2999_20Percent() {

        tireJob.setCustomerNumber(2999);
        tireJob.setDiscountRateCustomerType();
        double actual = tireJob.getDiscountRateCustomerType();
        assertEquals(0.2, actual);
    }    

    @Test
    public void testSetDiscountRateCustomerType_3000_0Percent() {

        tireJob.setCustomerNumber(3000);
        tireJob.setDiscountRateCustomerType();
        double actual = tireJob.getDiscountRateCustomerType();
        assertEquals(0.0, actual);
    }    

    @Test
    public void testSetDiscountRateCustomerType_4000_0Percent() {

        tireJob.setCustomerNumber(4000);
        tireJob.setDiscountRateCustomerType();
        double actual = tireJob.getDiscountRateCustomerType();
        assertEquals(0.0, actual);
    }    
    
}
